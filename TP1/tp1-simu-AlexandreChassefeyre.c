#include <stdio.h>
#include <stdlib.h>

// Question A

/* ----- Fonction middleSquareTechnique ----- */
/* Paramètre :                                */
/*      - int digits : la seed                */
/* Retourne :                                 */
/*      Résultat après utilisation de la      */
/*      technique "middle square"             */
int middleSquareTechnique(int digits)
{
    int res;

    res = digits * digits; // on prend le carré du nombre donné
    res = res / 100;       // on supprime les deux derniers chiffres du résultat
    res = res % 10000;     // on supprime les deux premiers chiffres du résultat

    return res;
}

/* ----- Fonction testMiddleSquareTechnique ----- */
/* Fonction de test de la fonction précédente     */
/* Paramètres :                                   */
/*      - int digits : la seed                    */
/*      - int nbIter : nombre d'itérations        */
/*      pour la simulation                        */
/* Affiche :                                      */
/*      Calcul et résultats                       */
void testMiddleSquareTechnique(int digits, int nbIter)
{
    printf("\n--- Test seed %d ---\n", digits);
    for (int i = 0; i < nbIter; i++)
    {
        printf("%d * %d = %d\n", digits, digits, digits * digits);
        digits = middleSquareTechnique(digits);
        printf("%d. %d\n", i + 1, digits);
    }
}

// Question B

/* --------- Fonction coinTossing --------- */
/* Fonction qui compte le nombre de faces   */
/* sur un nombre de lancés de pièces        */
/* Paramètre :                              */
/*      - int nb : nombre de lancés         */
/* Retourne :                               */
/*      Nombre de faces                     */
int coinTossing(int nb)
{
    int cptHeads = 0;
    int coin;

    for (int i = 0; i < nb; i++)
    {
        coin = (rand() % 2);
        if (coin == 0)
            cptHeads++;
    }

    return cptHeads;
}

/* --------- Fonction  diceSimulation --------- */
/* Fonction qui simule un lancé de dé à 6 faces */
/* Paramètre :                                  */
/*      - int nb : nombre de lancés             */
/* Affiche :                                    */
/*      Compteur pour chaque face               */
void diceSimulation(int nb)
{
    int dice[6] = {0};
    int index;

    for (int i = 0; i < nb; i++)
    {
        index = (rand() % 6);
        dice[index]++;
    }
    printf("1 : %d, 2 : %d, 3 : %d, 4 : %d, 5 : %d, 6 : %d\n", dice[0], dice[1], dice[2], dice[3], dice[4], dice[5]);
}

/* --------- Fonction tenFacesDiceSimulation --------- */
/* Fonction qui simule un lancé de dé à 10 face        */
/* Paramètre :                                         */
/*      - int nb : nombre de lancés                    */
/* Affiche :                                           */
/*      Compteur pour chaque face                      */
void tenFacesDiceSimulation(int nb)
{
    int dice[10] = {0};
    int index;

    for (int i = 0; i < nb; i++)
    {
        index = (rand() % 10);
        dice[index]++;
    }
    for (int j = 0; j < 10; j++)
    {
        printf("%d ", dice[j]);
    }
    printf("\n");
}

// Question C
int seed = 5; // seed pour la fonction intRand

/* ---------- Fonction intRand ---------- */
/* Fonction qui génère la trace attendue  */
/* Retourne :                             */
/*   seed déterminée pseudo-aléatoirement */
int intRand()
{
    seed = (5 * seed + 1) % 16;
    return seed;
}

// La fonction floatRand est similaire à intRand mais avec des floatant
float floatRand()
{
    return (float)intRand() / 16.0;
}

// La fonction intRand2 est similaire à intRand mais avec des paramètres modifiables
int intRand2(int a, int c, int m, int seed)
{
    seed = (a * seed + c) % m;
    return seed;
}

int main()
{
    // Test question A
    /*int N0 = 1234;
    printf("On donne la graine : %d\n", N0);
    printf("On obtient le nombre : %d\n", middleSquareTechnique(N0));
    testMiddleSquareTechnique(1234, 70);*/
    // testMiddleSquareTechnique(4100, 70);
    //  testMiddleSquareTechnique(3141, 70);

    // Test question B
    /*int heads;
    for (int i = 0; i < 10; i++)
    {
        heads = coinTossing(10);
        printf("10 lancés : %d faces et %d piles\n", heads, 10 - heads);
        heads = coinTossing(100);
        printf("100 lancés : %d faces et %d piles\n", heads, 100 - heads);
        heads = coinTossing(1000);
        printf("1000 lancés : %d faces et %d piles\n", heads, 1000 - heads);
    }*/

    /*for (int i = 0; i < 10; i++)
    {
        printf("Test sur 10 lancés de dé :\n");
        diceSimulation(10);
    }*/

    /*printf("Test sur 100 lancés de dé :\n");
    tenFacesDiceSimulation(100);
    printf("Test sur 1000 lancés de dé :\n");
    tenFacesDiceSimulation(1000);
    printf("Test sur 1000000 lancés de dé :\n");
    tenFacesDiceSimulation(1000000);*/

    // Test question C
    /*for (int i = 0; i < 32; i++)
    {
        printf("%d ", intRand());
    }*/

    /*for (int i = 0; i < 32; i++)
    {
        printf("%.4f ", floatRand());
    }*/

    return 0;
}

// Example of scientific libraries implementing up to date random number generators :
// GNU scientific Library or OpenRAND

// Example of statistical libraries able to test the quality of pseudo random number sources
// NIST Statistical Test Suite or TestU01