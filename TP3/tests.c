/* -------------------------------------------------------------------- */
/* Fichier contenant les fonctions de test des fonctions du fichier     */
/* tp3.c ainsi que la fonction main pour lancer les tests               */
/* -------------------------------------------------------------------- */

#include "tp3.h"

void testSimPi(int nbOfPoints)
{
    double pi = simPi(nbOfPoints);
    printf("Estimation de pi avec %d points: %.8f\n", nbOfPoints, pi);
}

void testMeanPi(int nbOfPoints, int n, double *experiments)
{
    double pi = meanPi(nbOfPoints, n, experiments);
    printf("Estimation de pi avec %d points et %d expériences: %.8f\n", nbOfPoints, n, pi);
    printf("Erreur absolue : %.8f\n", fabs(pi - M_PI));
    printf("Erreur relative : %.8f\n", fabs(1 - (pi / M_PI)));
}

void testConfidenceInterval(int nbExp, int nbOfPoints, double *experiments)
{
    double *interval = confidenceInterval(nbExp, nbOfPoints, experiments);
    printf("Intervalle de confiance : [%.5f,%.5f]", interval[0], interval[1]);
    free(interval);
}

int main()
{
    // Initialisation pour le générateur MT
    unsigned long init[4] = {0x123, 0x234, 0x345, 0x456}, length = 4;
    init_by_array(init, length);

    int nbExp = 40;
    // Tableau global contenant les expériences et servant dans plusieurs fonctions
    double *experiments = malloc(sizeof(double) * nbExp);

    // Question 1
    // testSimPi(1000);
    // testSimPi(1000000);
    // testSimPi(1000000000);

    // Question 2
    // testMeanPi(1000, 10, experiments);
    // testMeanPi(1000, 20, experiments);
    // testMeanPi(1000, 30, experiments);
    // testMeanPi(1000, 40, experiments);
    // testMeanPi(1000000, 10, experiments);
    // testMeanPi(1000000, 20, experiments);
    // testMeanPi(1000000, 30, experiments);
    // testMeanPi(1000000, 40, experiments);
    // testMeanPi(1000000000, 10, experiments);
    // testMeanPi(1000000000, 40, experiments);

    // Question 3
    // testConfidenceInterval(40, 1000000, experiments);
    // testConfidenceInterval(10, 1000000, experiments);
    // testConfidenceInterval(10, 1000000000, experiments);

    free(experiments);

    return 0;
}
