/* -------------------------------------------------------------------- */
/* Fichier d'entête contenant les importations de librairies,           */
/* les déclarations de constantes et les définitions de fonctions       */
/* utilisés dans les fichiers tp3.c et tests.c                          */
/* -------------------------------------------------------------------- */

#ifndef tp3
#define tp3

#define R_pi 1
#define C_infini 999999999
#include <stdio.h>
#include <math.h>
#include <stdlib.h>

// fonctions du générateur mt utilisées pour le TP2
void init_by_array(unsigned long init_key[], int key_length);
double genrand_real1(void);

// fonctions implémentées dans le TP3
double simPi(int nbOfPoints);
double meanPi(int nbOfPoints, int nbExp, double *experiments);
double *confidenceInterval(int nbExp, int nbOfPoints, double *experiments);

#endif
