/* -------------------------------------------------------------------- */
/* Fichier contenant les fonctions à implémenter pour répondre aux      */
/* questions du TP2                                                     */
/* -------------------------------------------------------------------- */

#include "tp3.h"

// Question 1

/* -------------------------------------------------------------------- */
/* simPi : Estime une valeur de pi                                      */
/*                                                                      */
/* En entrée: le nombre de points à tirer                               */
/*                                                                      */
/* En sortie: Une estimation de pi avec une précision différente selon  */
/* le nombre de points                                                  */
/* -------------------------------------------------------------------- */
double simPi(int nbOfPoints)
{

    double x, y;
    double nbPointsInside = 0;
    double pi_estimation;

    for (int i = 0; i < nbOfPoints; i++)
    {
        x = genrand_real1();
        y = genrand_real1();

        if ((x * x + y * y) < R_pi * R_pi)
        {
            nbPointsInside++;
        }
    }

    pi_estimation = 4 * (nbPointsInside / nbOfPoints);

    return pi_estimation;
}

// Question 2

/* -------------------------------------------------------------------- */
/* meanPi : Calcul la moyenne des estimations de pi                     */
/*                                                                      */
/* En entrée: le nombre de points à tirer, le nombre d'expériences      */
/*                                                                      */
/* En sortie: Une estimation de pi avec une précision différente selon  */
/* le nombre de points et le nombre d'expériences                       */
/* -------------------------------------------------------------------- */
double meanPi(int nbOfPoints, int nbExp, double *experiments)
{
    double sum = 0;

    for (int i = 0; i < nbExp; i++)
    {
        experiments[i] = simPi(nbOfPoints);
    }

    for (int j = 0; j < nbExp; j++)
    {
        sum += experiments[j];
    }

    return sum / nbExp;
}

// tab global ou en paramètre

/* -------------------------------------------------------------------- */
/* confidenceIntervals : Calcul un rayon de confiance pour pi           */
/*                                                                      */
/* En entrée: le nombre de points à tirer, le nombre d'expériences      */
/*                                                                      */
/* En sortie: Un intervalle de confiance à 95% sur la valeur de pi      */
/* -------------------------------------------------------------------- */
double *confidenceInterval(int nbExp, int nbOfPoints, double *experiments)
{
    double student[30] = {12.706, 4.303, 3.182, 2.776, 2.571, 2.447, 2.365, 2.308, 2.262, 2.228,
                          2.201, 2.179, 2.160, 2.145, 2.131, 2.120, 2.110, 2.101, 2.093, 2.086,
                          2.080, 2.074, 2.069, 2.064, 2.060, 2.056, 2.052, 2.048, 2.045, 2.042};
    double X_mean = meanPi(nbOfPoints, nbExp, experiments);
    double S2_n = 0;
    double R;
    double *interval = malloc(sizeof(double) * 2);
    double t;

    // choix du t dans la table de student
    if (nbExp <= 30)
    {
        t = student[nbExp - 1];
    }
    else
    {
        if (nbExp == 40)
            t = 2.021;
        else if (nbExp == 80)
            t = 2.000;
        else if (nbExp == 120)
            t = 1.980;
        else if (nbExp >= C_infini)
            t = 1.960;
    }

    // caclul de la variance non biaisée
    for (int i = 0; i < nbExp; i++)
    {
        S2_n += pow(experiments[i] - X_mean, 2);
    }
    S2_n /= nbExp - 1;

    // calcul du rayon de confiance
    R = t * sqrt(S2_n / nbExp);

    // calcul de l'intervalle de confiance
    interval[0] = X_mean - R;
    interval[1] = X_mean + R;

    return interval;
}