/* -------------------------------------------------------------------- */
/*                                                                      */
/* SimpleModel.java : Ce fichier contient le modèle très simplifié de   */
/* la croissance de population de lapins                                */
/*                                                                      */
/* -------------------------------------------------------------------- */

public class SimpleModel {
    public static void main(String[] args) {
        int nbMonths = 12;
        int nbCoupleKittens = 1;
        int nbCoupleAdults = 0;
        int population = 1;

        System.out.println("------ Modèle simple ------");

        for (int m = 1; m <= nbMonths; m++) {
            System.out.println("Mois " + m + " : " + population);

            int nbBirths = nbCoupleAdults;  // chaque couple fait 2 enfants
            nbCoupleAdults += nbCoupleKittens;  // chaque couple d'enfants deviens adulte
            nbCoupleKittens = nbBirths;

            population = nbCoupleAdults + nbCoupleKittens;
        }

        System.out.println("------ Modèle de Leonardo of Pizza ------");

        for (int i = 1; i <= nbMonths; i++) {
            System.out.println("Mois " + i + ": " + Fibonacci.fibo(i));
        }
    }
}
