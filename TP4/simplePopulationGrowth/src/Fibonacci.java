/* -------------------------------------------------------------------- */
/*                                                                      */
/* Fibonnacci.java : Ce fichier contient la méthode qui simule le       */
/* modèle proposé par Leonardo of Pizza                                 */
/*                                                                      */
/* -------------------------------------------------------------------- */

public class Fibonacci {
    public static long fibo(int n) {
        if (n <= 1) return n;
        else return fibo(n - 1) + fibo(n - 2);
    }
}