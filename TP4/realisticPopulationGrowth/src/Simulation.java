/* -------------------------------------------------------------------- */
/*                                                                      */
/* Simulation.java : Ce fichier contient la méthode main qui perme      */
/* d'instancier des nouveaux mondes et simuler l'évolution de la        */
/* population de lapin dans ce monde                                    */
/*                                                                      */
/* -------------------------------------------------------------------- */

import java.io.FileWriter;
import java.io.IOException;

class Simulation {

/* -------------------------------------------------------------------- */
/* exportToCSV(int[], String) : Permet d'exporter les données du tableau*/
/* au format CSV                                                        */
/*                                                                      */
/* Importe les données dans un fichier CSV pour les traiter depuis un   */
/* tableau excel                                                        */
/* -------------------------------------------------------------------- */
    public static void exportToCSV(int[] array, String fileName) {

        try (FileWriter writer = new FileWriter(fileName)) {
            for (int i = 0; i < array.length; i++) {
                writer.append(String.valueOf(array[i]));
                if (i < array.length - 1) {
                    writer.append(",");
                }
            }
            writer.append("\n");
            System.out.println("Données exportées dans " + fileName);
        } catch (IOException e) {
            System.err.println("Erreur lors de l'écriture du fichier CSV: " + e.getMessage());
        }
    }

/* -------------------------------------------------------------------- */
/* main() : Méthode principale permettant de lancer le programme        */
/*                                                                      */
/* Lance les simulations et récupère les données                        */
/* -------------------------------------------------------------------- */
    public static void main(String[] args) {
        int nbCouplesRabbits = 10;
        int ageRabbits = 50;
        int nbMonths = 60;
        int nbSimu = 30;

        int [] avgMales = new int[nbMonths];
        int [] avgFemales = new int[nbMonths];
        int [] avgKittens = new int[nbMonths];
        int [] avgDeaths = new int[nbMonths];
        int [] avgPopulation = new int[nbMonths]; 

        int [] stats = new int[4];

        // init à 0
        for (int m = 0; m < nbMonths; m++) {
            avgMales[m] = 0;
            avgFemales[m] = 0;
            avgKittens[m] = 0;
            avgDeaths[m] = 0;
            avgPopulation[m] = 0;
       }

       // simulation
       for (int s = 0; s < nbSimu; s++) {
            System.out.println("------ Simulation " + (s + 1) + " ------");

            World world = new World(nbCouplesRabbits, ageRabbits);

            for (int m = 0; m < nbMonths; m++) {
                //System.out.println("Mois numero : " + (m + 1));
                world.advanceTime();
                world.printStats();

                // récupération des données pour le mois en cours
                stats = world.getStats();
                avgMales[m] += stats[0];
                avgFemales[m] += stats[1];
                avgKittens[m] += stats[2];
                avgDeaths[m] += stats[3];
                avgPopulation[m] += stats[0] + stats[1] + stats[2];
            }
       }

       // moyenne
       for (int m = 0; m < nbMonths; m++) {
            avgMales[m] /= nbSimu;
            avgFemales[m] /= nbSimu;
            avgKittens[m] /= nbSimu;
            avgDeaths[m] /= nbSimu;
            avgPopulation[m] /= nbSimu;
       }

       // export des données
       exportToCSV(avgPopulation, "stats/population.csv");
       exportToCSV(avgMales, "stats/males.csv");
       exportToCSV(avgKittens, "stats/kittens.csv");
       exportToCSV(avgFemales, "stats/females.csv");
       exportToCSV(avgDeaths, "stats/deaths.csv");
    }
}