/* -------------------------------------------------------------------- */
/*                                                                      */
/* World.java : Ce fichier contient les méthodes liés au monde et       */
/* notamment la population de lapin présente dans ce monde              */
/*                                                                      */
/* -------------------------------------------------------------------- */

import java.util.*;

public class World {
    // Liste contenant tous les lapins
    private ArrayList<Rabbit> rabbitPopulation = new ArrayList<>();
    private boolean isMaleLeft;
    private int nbMonths;
    private int nbDeaths;

/* -------------------------------------------------------------------- */
/* World() : Constructeur de la classe World                            */
/*                                                                      */
/* Initialise un monde avec un nombre de mois et un nombre de couples   */
/* de lapins au départ                                                  */
/* -------------------------------------------------------------------- */
    World(int nbCouplesRabbits, int age) {
        initPopulation(nbCouplesRabbits, age);
        nbMonths = 0;
    }

/* -------------------------------------------------------------------- */
/* initPopulation() : Initialise la population de lapins                */
/*                                                                      */
/* Ajoute un male et une femelle autant de fois qu'il y a de couples au */
/* départ                                                               */
/* -------------------------------------------------------------------- */
    private void initPopulation(int nbCouplesRabbits, int age) {
        for (int i = 0; i < nbCouplesRabbits; i++) {
            this.rabbitPopulation.add(new Rabbit(age));
            this.rabbitPopulation.add(new Female(age));
        }
    }

/* -------------------------------------------------------------------- */
/* deleteDeadRabbits() : Supprime les lapins morts de la population     */
/*                                                                      */
/* Supprime les lapins morts de la liste pour que le parcours soit plus */
/* rapide ensuite                                                       */
/* -------------------------------------------------------------------- */
    private void deleteDeadRabbits() {
        this.nbDeaths = 0;
        Iterator<Rabbit> iterator = this.rabbitPopulation.iterator();
        while (iterator.hasNext()) {
            Rabbit rabbit = iterator.next();
            if (!rabbit.isRabbitAlive()) {
                this.nbDeaths++;
                iterator.remove();
            }
        }
    }

/* -------------------------------------------------------------------- */
/* isMaleLeft() : Vérifie si il reste des males (adultes) dans la       */
/* population de lapins                                                 */
/*                                                                      */
/* Retourne true si il reste au moins un male et false sinon            */
/* -------------------------------------------------------------------- */
    private boolean isMaleLeft() {
        boolean male = false;

        for (Rabbit rabbit : this.rabbitPopulation) {
            if (!(rabbit instanceof Female) && rabbit.isRabbitAdult()) {
                male = true;
                break;
            }
        }

        if (!male) System.out.println("Plus de males dans la population\n");

        return male;
    }

/* -------------------------------------------------------------------- */
/* advanceTime() : Permet de simuler l'avancement du temps              */
/*                                                                      */
/* Fait avancer le temps d'un mois                                      */
/* -------------------------------------------------------------------- */
    public void advanceTime() {
        this.nbMonths++;
        this.isMaleLeft = isMaleLeft();
        
        int nbRabbits = this.rabbitPopulation.size();
        for (int i = 0; i < nbRabbits; i++) {
            
            Rabbit rabbit = this.rabbitPopulation.get(i);
            if (rabbit instanceof Female) {
                Female female = (Female) rabbit;
                female.toAge();

                if (female.isRabbitAlive() && female.isRabbitAdult() && this.isMaleLeft) {
                    // traiter les naissances
                    if (female.isGivingBirth()) {
                        int nbKittens = female.getNbKittens();
                        // création des enfants
                        for (int j = 0; j < nbKittens; j++) {
                            double rand = MTRandom.nextDouble();
                            if (rand > 0.5) this.rabbitPopulation.add(new Rabbit());
                            else this.rabbitPopulation.add(new Female());
                        }
                    }
                }
            }
            else {
                rabbit.toAge(); // le lapin grandit
            }
            rabbit.isDying(); // le lapin survit ou non
        }
        this.deleteDeadRabbits();
    }


/* -------------------------------------------------------------------- */
/* printStats() : Permet d'afficher les statistiques du mois en cours   */
/*                                                                      */
/* Affiche différents statistiques à propos de la population            */
/* -------------------------------------------------------------------- */
    public void printStats() {
        int nbMales = 0;
        int nbFemales = 0;
        int nbKittens = 0;

        for (Rabbit rabbit : this.rabbitPopulation) {
            if (rabbit.isRabbitAdult()) {
                if (rabbit instanceof Female) {
                    nbFemales++;
                }
                else nbMales++;
            }
            else nbKittens++;
        }

        System.out.println("Month : " + this.nbMonths);
        System.out.println("Males : " + nbMales);
        System.out.println("Females : " + nbFemales);
        System.out.println("Kittens : " + nbKittens);
        System.out.println("NbDeaths : " + this.nbDeaths);
        System.out.println("------------------------------");
    }

/* -------------------------------------------------------------------- */
/* getStats() : Permet de récupérer les statistiques du mois en cours   */
/*                                                                      */
/* Retourne les différents statistiques à propos de la population dans  */
/* une liste                                                            */
/* -------------------------------------------------------------------- */
    public int[] getStats() {

        int[] stats= new int[4];
        stats[0] = 0;   // nb de males
        stats[1] = 0;   // nb de femelles
        stats[2] = 0;   // nb de bébés
        stats[3] = this.nbDeaths;

        for (Rabbit rabbit : this.rabbitPopulation) {
            if (rabbit.isRabbitAdult()) {
                if (rabbit instanceof Female) {
                    stats[1]++;
                }
                else stats[0]++;
            }
            else stats[2]++;
        }

        return stats;
    }
}