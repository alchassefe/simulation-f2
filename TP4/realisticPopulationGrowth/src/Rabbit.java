/* -------------------------------------------------------------------- */
/*                                                                      */
/* Rabbit.java : Ce fichier contient les méthodes et attributs d'un     */
/* lapin                                                                */
/*                                                                      */
/* -------------------------------------------------------------------- */

public class Rabbit {
    protected int age;  // en mois
    protected boolean isAdult;
    protected boolean isAlive;
    protected int sexualMaturity;   // en mois
    protected double annualSurvivalRate; // en pourcents
    protected double monthlySurvivalRate;

/* -------------------------------------------------------------------- */
/* Rabbit() : Constructeur de la classe Rabbit                          */
/*                                                                      */
/* Initialise un objet Lapin avec les attributs d'un jeune lapin        */
/* -------------------------------------------------------------------- */
    Rabbit() {
        this.age = 0;
        this.isAdult = false;
        this.isAlive = true;
        this.annualSurvivalRate = 0.35;
        this.monthlySurvivalRate = annualToMonthly(annualSurvivalRate);
        // tirage aléatoire pour l'âge de maturité (entre 5 et 8 mois)
        this.sexualMaturity = MTRandom.nextInt(4) + 5;
    }

/* -------------------------------------------------------------------- */
/* Rabbit(int age) : Constructeur de la classe Rabbit                   */
/*                                                                      */
/* Initialise un objet Lapin avec les attributs d'un lapin adulte et    */
/* l'âge donné en paramètre                                             */
/* -------------------------------------------------------------------- */
Rabbit(int age) {
    if (age < 12 || age > 120) {
        throw new IllegalArgumentException("Age donné invalide");
    }
    this.age = age;
    this.isAdult = true;
    this.isAlive = true;
    this.sexualMaturity = 12;
    this.annualSurvivalRate = 0.60;
    this.monthlySurvivalRate = annualToMonthly(annualSurvivalRate);
}

/* -------------------------------------------------------------------- */
/* annualToMonthly() : Convertit le taux de survie annuel en taux de    */
/* survie mensuel                                                       */
/*                                                                      */
/* -------------------------------------------------------------------- */   
    private double annualToMonthly(double annualRate) {
        return Math.pow(annualRate, 1.0 / 12);
    }

/* -------------------------------------------------------------------- */
/* toAge() : Fait vieillir un lapin                                     */
/*                                                                      */
/* Vérifie si le lapin a atteint son âge de maturité et modifie son     */
/* taux de survie selon son âge                                         */
/* -------------------------------------------------------------------- */
    public void toAge() {
        this.age++;

        // augmentation du taux de survie quand le lapin devient adulte
        if (this.age == this.sexualMaturity) {
            this.isAdult = true;
            this.annualSurvivalRate = 0.60;
            this.monthlySurvivalRate = annualToMonthly(annualSurvivalRate);
        }

        // diminution du taux de survie chaque année après 10 ans
        if (this.age >= 10*12 && this.age % 12 == 0) {
            this.annualSurvivalRate -= 0.10;
            this.monthlySurvivalRate = annualToMonthly(annualSurvivalRate);
            if (this.annualSurvivalRate <= 0) {
                this.isAlive = false;
            }
        }
    }

/* -------------------------------------------------------------------- */
/* isDying() : Vérifie si un lapin doit mourir ou non                   */
/*                                                                      */
/* Met à jour l'attribut isAlive du lapin si il n'a pas survécu         */
/* -------------------------------------------------------------------- */
    public void isDying() {
        double rand;

        // Tirage aléatoire pour déterminer si un lapin survit ou non
        rand = MTRandom.nextDouble();
        if (this.monthlySurvivalRate < rand) {
            this.isAlive = false;
        }
    }

/* -------------------------------------------------------------------- */
/* isRabbitAlive() : Vérifie si un lapin est toujours vivant            */
/*                                                                      */
/* Valeur de retour : true si le lapin est vivant et false sinon        */
/* -------------------------------------------------------------------- */
    public boolean isRabbitAlive() {
        return this.isAlive;
    }

/* -------------------------------------------------------------------- */
/* isRabbitAdult() : Vérifie si un lapin a atteint sa maturité sexuelle */
/*                                                                      */
/* Valeur de retour : true si le lapin est adulte et false sinon        */
/* -------------------------------------------------------------------- */
    public boolean isRabbitAdult() {
        return this.isAdult;
    }
}