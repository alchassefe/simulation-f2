/* -------------------------------------------------------------------- */
/*                                                                      */
/* Female.java : Ce fichier contient les méthodes et attributs d'un     */
/* lapin femelle qui hérite de la classe lapin                          */
/*                                                                      */
/* -------------------------------------------------------------------- */

public class Female extends Rabbit {
    private int nbLitters; // nb de portées dans l'année
    private int actualNbLitters; // nb de portées actuel
    private int nbKittens;  // nb de bébés dans la portée

    // tableau représentant les probabilités cumulées du nombre de portées par an
    private static double[] probaNbLitters = {0.1, 0.2, 0.4, 0.6, 0.8, 0.9, 1};

/* -------------------------------------------------------------------- */
/* Female() : Constructeur de la classe Female qui hérite de Rabbit     */
/*                                                                      */
/* Initialise un objet Femelle avec les attributs d'un jeune lapin      */
/* -------------------------------------------------------------------- */
    Female() {
        super();
    }

/* -------------------------------------------------------------------- */
/* Female(int age) : Constructeur de la classe Female qui hérite de la  */
/* classe Rabbit                                                        */
/*                                                                      */
/* Initialise un objet Femelle avec les attributs d'un lapin adulte et  */
/* avec l'âge donné en paramètre                                        */
/* -------------------------------------------------------------------- */
    Female(int age) {
        super(age);
    }


/* -------------------------------------------------------------------- */
/* toAge() : Fait vieillir une femelle                                  */
/*                                                                      */
/* Initialise le nombre de portées et d'enfants chaque année et appelle */
/* la méthode toAge() de la classe Rabbit                               */
/* -------------------------------------------------------------------- */
    @Override
    public void toAge() {
        // tirage du nombre de portées dans l'année
        if (this.age % 12 == 0) {
            initNbLitters();
        }

        super.toAge();
    }

/* -------------------------------------------------------------------- */
/* isGivingBirth() : Vérifie si une femelle peut accoucher ce mois-ci   */
/*                                                                      */
/* Calcul la probabilité que la femelle puisse accoucher en fonction du */
/* nombre de portées et de mois restants                                */
/* -------------------------------------------------------------------- */
    public boolean isGivingBirth() {
        boolean birth = false;
        double probaGiveBirth;

        if (!isNbLittersReached()) {
            double rand = MTRandom.nextDouble();
            // nombre de portées restantes sur nombre de mois restants
            probaGiveBirth = (this.nbLitters - this.actualNbLitters) / (12 - (this.age % 12)); 
            if (probaGiveBirth >= rand) {
                birth = true;
                this.actualNbLitters ++;
                initNbKittens();
            }
        }

        return birth;
    }

/* -------------------------------------------------------------------- */
/* isNbLittersReached() : Vérifie si le nombre de portées sur l'année a */
/* été atteint                                                          */
/*                                                                      */
/* Valeur de retour : true si le nombre de portées maximum est atteint  */
/* et false sinon                                                       */
/* -------------------------------------------------------------------- */
    private boolean isNbLittersReached() {
        return this.actualNbLitters == nbLitters;
    }

/* -------------------------------------------------------------------- */
/* getNbKittens() : Donne le nombre de bébés lapins dans une portée     */
/*                                                                      */
/* Valeur de retour : le nombre de lapins dans la portée du mois actuel */
/* -------------------------------------------------------------------- */
    public int getNbKittens() {
        return this.nbKittens;
    }

/* -------------------------------------------------------------------- */
/* initNbLitters() : Détermine le nombre de portées dans l'année         */
/*                                                                      */
/* Calcul aléatoirement le nombre de portées de la femelle dans l'année */
/* entre 3 et 9 avec plus de chance d'avoir 5, 6 ou 7                   */
/* -------------------------------------------------------------------- */
    private void initNbLitters() {
        this.actualNbLitters = 0;

        // tirage d'un nombre entre 0 et 1 pour comparer avec les probabilités représentées dans le tableau
        double rand = MTRandom.nextDouble();

        this.nbLitters = 3;
        int indice = 0;

        while(rand > probaNbLitters[indice]) {
            this.nbLitters++;
            indice++;
        }
    }

/* -------------------------------------------------------------------- */
/* initNbKittens() : Détermine le nombre de bébés lapins dans une portée */
/*                                                                      */
/* Calcul aléatoirement le nombre de bébés dans une portée de la femelle*/
/* de manière uniforme entre 3 et 6                                     */
/* -------------------------------------------------------------------- */
    private void initNbKittens() {
        // tirage d'un nombre entre 0 et 3 de manière uniforme
        this.nbKittens = MTRandom.nextInt(3) + 3;
    }
}
