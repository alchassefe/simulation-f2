/* -------------------------------------------------------------------- */
/* Fichier contenant les fonctions à implémenter pour répondre aux      */
/* questions du TP2                                                     */
/* -------------------------------------------------------------------- */

#include "tp2.h"

// Question 2

/* -------------------------------------------------------------------- */
/* uniform : Génère un nombre entre a et b                              */
/*                                                                      */
/* En entrée: a et b deux nombre réels                                  */
/*                                                                      */
/* En sortie: un nombre réel pseudo-aléatoire entre a et b              */
/* -------------------------------------------------------------------- */
double uniform(double a, double b)
{
    return a + (b - a) * genrand_real1();
}

// Question 3 A

/* -------------------------------------------------------------------- */
/* empiricalDistribution : simule la distribution avec 3 classes        */
/*                                                                      */
/* En entrée: drawings, le nombre de tirages                            */
/*                                                                      */
/* En sortie: un tableau contenant le pourcentage obtenu pour chaque    */
/* classe                                                               */
/* -------------------------------------------------------------------- */
double *empiricalDistribution(int drawings)
{
    double *pdf = calloc(3, sizeof(double));
    double percentage;

    // détermine la classe du nombre à chaque tirage
    for (int i = 0; i < drawings; i++)
    {
        percentage = genrand_real1();
        if (percentage < 0.35)
            pdf[0] += 1;
        else if (percentage < 0.80)
            pdf[1] += 1;
        else
            pdf[2] += 1;
    }
    // calcul le pourcentage pour chaque classe
    for (int j = 0; j < 3; j++)
    {
        pdf[j] = pdf[j] / drawings;
    }

    return pdf;
}

// Question 3 B

/* -------------------------------------------------------------------- */
/* genericEmpiricalDistribution : simule la distribution empirique avec */
/* un nombre de classes donné                                           */
/*                                                                      */
/* En entrée: nbClasses le nombre de classes, obs un tableau contenant  */
/* le nombre d'observations pour chaque classes                         */
/*                                                                      */
/* En sortie: un tableau contenant le pourcentage cumulé obtenu pour    */
/* chaque classe                                                        */
/* -------------------------------------------------------------------- */
double *genericEmpiricalDistribution(int nbClasses, double obs[])
{
    double total = 0;                                        // nombre total d'individus
    double *pdf = malloc(sizeof(double) * nbClasses);        // tableau des probabilités de chaque classe
    double *cumulProba = malloc(sizeof(double) * nbClasses); // tableau des probabilités cumulées
    double cumul = 0;

    // compte le nombre total d'observations
    for (int i = 0; i < nbClasses; i++)
    {
        total += obs[i];
    }
    // calcul le pourcentage cumulé pour chaque classe
    for (int j = 0; j < nbClasses; j++)
    {
        pdf[j] = obs[j] / total;
        cumul += pdf[j];
        cumulProba[j] = cumul;
    }

    free(pdf);

    return cumulProba;
}

// Question 4 A

/* -------------------------------------------------------------------- */
/* negExp : simule la distribution continue                             */
/*                                                                      */
/* En entrée: mean la moyenne attendue                                  */
/*                                                                      */
/* En sortie: un nombre pseudo aléatoire d'un côté ou de l'autre de la  */
/* moyenne donnée                                                       */
/* -------------------------------------------------------------------- */
double negExp(double mean)
{
    return (-mean) * log(1 - genrand_real1());
}

// Question 4 C

/* -------------------------------------------------------------------- */
/* distributionFrequency : vérifie la fréquence d'apparition entre      */
/* chaque nombre                                                        */
/*                                                                      */
/* En entrée: drawings le nombre de tirages                             */
/*                                                                      */
/* En sortie: un tableau contenant le nombre d'apparitions pour chaque  */
/* nombre entre 0 et 20 (les autres apparitions dans la dernière case)  */
/* -------------------------------------------------------------------- */
int *distributionFrequency(int drawings)
{
    int *test22bins = malloc(sizeof(int) * 22);
    double randNegExp;

    for (int i = 0; i < drawings; i++)
    {
        randNegExp = negExp(10);
        if (randNegExp > 20)
            test22bins[21]++;
        else
        {
            test22bins[(int)randNegExp]++;
        }
    }

    return test22bins;
}

// Question 5.1

/* -------------------------------------------------------------------- */
/* diceSimulation: simule 40 lancés de dés et calcul la somme obtenue   */
/*                                                                      */
/* En entrée: drawings le nombre de tirages                             */
/*                                                                      */
/* En sortie: un tableau contenant pour chaque somme possible le        */
/* nombre d'apparitions après simulations                               */
/* -------------------------------------------------------------------- */
int *diceSimulation(int drawings)
{
    int sum;
    int *diceResult = calloc(241, sizeof(int));

    // calcul la somme des 40 lancés
    for (int i = 0; i < drawings; i++)
    {
        sum = 0;
        for (int j = 0; j < 40; j++)
        {
            sum += genrand_int32() % 6 + 1;
        }
        diceResult[sum] += 1;
    }

    return diceResult;
}

// Question 5.2

/* -------------------------------------------------------------------- */
/* gaussianDistribution: simule le modèle Box and Muller                */
/*                                                                      */
/* En entrée: drawings le nombre de tirages                             */
/*                                                                      */
/* En sortie: un tableau contenant pour chaque résultat possible le     */
/* nombre d'apparitions après simulations                               */
/* -------------------------------------------------------------------- */
int *gaussianDistribution(int drawings)
{
    double Rn1, Rn2, x1, x2;
    int index1, index2;
    int *distribution = calloc(20, sizeof(int));

    // Remplissage du tableau représentant la distribution
    for (int i = 0; i < drawings / 2; i++)
    {
        // Génération des nombres pseudo-aléatoires
        Rn1 = genrand_real1();
        Rn2 = genrand_real1();

        // Equations de Box-Muller
        x1 = cos(2 * M_PI * Rn2) * sqrt(-2 * log(Rn1));
        x2 = sin(2 * M_PI * Rn2) * sqrt(-2 * log(Rn1));

        // intervalle de 0.5 entre -5 et 5
        if (x1 >= -5 && x1 <= 5)
        {
            index1 = (int)((x1 + 5) / 0.5);
            distribution[index1] += 1;
        }
        if (x2 >= -5 && x2 <= 5)
        {
            index2 = (int)((x2 + 5) / 0.5);
            distribution[index2] += 1;
        }
    }

    return distribution;
}