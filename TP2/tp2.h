/* -------------------------------------------------------------------- */
/* Fichier d'entête contenant les importations de librairies,           */
/* les déclarations de constantes et les définitions de fonctions       */
/* utilisés dans les fichiers tp2.c et tests.c                          */
/* -------------------------------------------------------------------- */

#ifndef tp2
#define tp2

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define M_PI 3.14159265358979323846

// fonctions du générateur mt utilisées pour le TP2
void init_by_array(unsigned long init_key[], int key_length);
unsigned long genrand_int32(void);
double genrand_real1(void);
double genrand_real2(void);

// fonctions implémentées dans le TP2
double uniform(double a, double b);
double *empiricalDistribution(int drawings);
double *genericEmpiricalDistribution(int nbClasses, double obs[]);
double negExp(double mean);
int *distributionFrequency(int drawings);
int *diceSimulation(int drawings);
int *gaussianDistribution(int drawings);

#endif