/* -------------------------------------------------------------------- */
/* Fichier contenant les fonctions de test des fonctions du fichier     */
/* tp2.c ainsi que la fonction main pour lancer les tests               */
/* -------------------------------------------------------------------- */

#include "tp2.h"

void testUniform(int drawings, double a, double b)
{
    for (int i = 0; i < drawings; i++)
    {
        printf("%.1f ", uniform(a, b));
        if (i % 5 == 4)
            printf("\n");
    }
}

void testEmpiricalDistribution(int drawings, int nbSimu)
{
    double *pdf;

    for (int i = 0; i < nbSimu; i++)
    {
        pdf = empiricalDistribution(drawings);
        printf("Classe A : %.2f  ", pdf[0]);
        printf("Classe B : %.2f  ", pdf[1]);
        printf("Classe C : %.2f  ", pdf[2]);
        printf("\n");
        free(pdf);
    }
    printf("\n");
}

// Question 3 C

void testGenericEmpiricalDistribution(int drawings, int nbClasses, double obs[])
{
    double percentage;
    double *pdf = calloc(nbClasses, sizeof(double));
    double *cumul;
    int i, j, k;

    cumul = genericEmpiricalDistribution(nbClasses, obs);

    // calcul du pourcentage de chaque classe après les tirages
    for (i = 0; i < drawings; i++)
    {
        percentage = genrand_real1();
        j = 0;
        while (percentage > cumul[j])
            j++;
        pdf[j] += 1;
    }
    for (k = 0; k < nbClasses; k++)
    {
        pdf[k] = pdf[k] / drawings;
    }

    for (int c = 0; c < 6; c++)
    {
        printf("Classe %d : %.2f  ", c, pdf[c]);
    }
    printf("\n");

    free(pdf);
    free(cumul);
}

// Question 4 B

double testNegExp(int drawings, int mean)
{
    double sum = 0;

    for (int i = 0; i < drawings; i++)
    {
        sum += negExp(mean);
    }

    return sum / drawings;
}

// Question 4 C

void testFrequency(int drawings)
{
    int *test22bins;

    test22bins = distributionFrequency(drawings);

    for (int i = 0; i < 22; i++)
    {
        printf("%d ", test22bins[i]);
    }
    printf("\n");

    free(test22bins);
}

void testDiceSimulation(int drawings)
{
    int *diceResult;

    diceResult = diceSimulation(drawings);

    for (int i = 0; i < 241; i++)
    {
        printf("%d\n", diceResult[i]);
    }

    free(diceResult);
}

void testGaussianDistribution(int drawings)
{
    int *distribution;

    distribution = gaussianDistribution(drawings);

    for (int i = 0; i < 20; i++)
        printf("%d\n", distribution[i]);

    free(distribution);
}

int main(void)
{
    // Initialisation pour le générateur MT
    unsigned long init[4] = {0x123, 0x234, 0x345, 0x456}, length = 4;
    init_by_array(init, length);

    // --- Question 1 --- Test des fonctions

    /*printf("1000 outputs of genrand_int32()\n");
    for (int i = 0; i < 1000; i++)
    {
        printf("%10lu ", genrand_int32());
        if (i % 5 == 4)
            printf("\n");
    }
    printf("\n1000 outputs of genrand_real2()\n");
    for (int i = 0; i < 1000; i++)
    {
        printf("%10.8f ", genrand_real2());
        if (i % 5 == 4)
            printf("\n");
    }*/

    // --- Question 2 ---

    // testUniform(100, -98, 57.7);

    // --- Question 3 A ---

    /*testEmpiricalDistribution(1000, 10);
    testEmpiricalDistribution(10000, 10);
    testEmpiricalDistribution(100000, 10);
    testEmpiricalDistribution(1000000, 10);*/

    // --- Question 3 C ---

    // Test with HDL ‘good’ cholesterol example
    /*double obs[6];
    obs[0] = 100;
    obs[1] = 400;
    obs[2] = 600;
    obs[3] = 400;
    obs[4] = 100;
    obs[5] = 200;
    testGenericEmpiricalDistribution(1000, 6, obs);
    testGenericEmpiricalDistribution(1000000, 6, obs);*/

    // --- Question 4 B ---

    /*printf("%.2f\n", testNegExp(1000, 10));
    printf("%.2f\n", testNegExp(1000000, 10));*/

    // --- Question 4 C ---

    // testFrequency(1000);
    // testFrequency(1000000);

    // --- Question 5.1 ---

    // testDiceSimulation(1000);
    // testDiceSimulation(1000000);

    // --- Question 5.2 ---

    // testGaussianDistribution(1000);
    // testGaussianDistribution(1000000);

    return 0;
}